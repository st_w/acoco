﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace SL
{
    class SL
    {

        /*
          The parts of the code generator should be implemented in the following order:
            1. Code buffer management
            2. Register management
            3. Management of code generation "operands"
            4. Methods for generating machine instructions in different formats
            5. Loading of values and addresses
            6. Translation of expressions
            7. Translation of statements
            8. Management of jumps
            9. Translation of procedure calls
            10. Generation of the object file
         */

        static void Main(string[] args)
        {
            Console.SetOut(new ToDebugWriter());

            string file = "../../example.sl";
            Scanner scanner = new Scanner(file);
            Parser parser = new Parser(scanner);
            parser.Parse();
            Console.WriteLine(parser.errors.count + " errors detected");
            
            parser.code.write(File.Open("out.bin", FileMode.Create));

            System.Diagnostics.Debug.WriteLine("");
            System.Diagnostics.Debug.WriteLine("");
            parser.code.write(new hexOutputStream(null));
            System.Diagnostics.Debug.WriteLine("");
            System.Diagnostics.Debug.WriteLine("");
            
            //Console.ReadKey();
        }


    }

    class ToDebugWriter : StringWriter
    {
        public override void WriteLine(string value)
        {
            Debug.WriteLine(value);
            base.WriteLine(value);
        }
    }


    class hexOutputStream : Stream {

        private Stream s;

        public hexOutputStream(Stream s)
        {
            this.s = s;
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {
        }

        public override long Length
        {
            get { return 0; }
        }

        public override long Position {get {return 0;} set {} }
        
        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            int cnt = 0;
            foreach (byte b in buffer) {
                if (cnt > count) break;
                if (s != null)
                {
                    char[] c = b.ToString("X2").ToCharArray();
                    s.WriteByte((byte)c[0]);
                    s.WriteByte((byte)c[1]);
                }
                else
                {
                    System.Diagnostics.Debug.Write(b.ToString("X2"));
                }
                cnt++;
            }
        }
    }


}
