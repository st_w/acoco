using SL.codegen;
using SL.symtab;
using StringList = System.Collections.Generic.List<string>;
using OperandList = System.Collections.Generic.List<SL.codegen.Operand>;
using System.IO;

// Set the name of your grammar here (and at the end of this grammar):


using System;

namespace SL {



public class Parser {
	public const int _EOF = 0;
	public const int _ident = 1;
	public const int _number = 2;
	public const int _charCon = 3;
	public const int maxT = 34;

	const bool _T = true;
	const bool _x = false;
	const int minErrDist = 2;
	
	public Scanner scanner;
	public Errors  errors;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;

public Code code = null;
Tab tab = null;
private Obj curMeth = null;


// If you want your generated compiler case insensitive add the
// keyword IGNORECASE here.




	public Parser(Scanner scanner) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (string msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) { ++errDist; break; }

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	bool StartOf (int s) {
		return set[s, la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}


	bool WeakSeparator(int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) {Get(); return true;}
		else if (StartOf(repFol)) {return false;}
		else {
			SynErr(n);
			while (!(set[syFol, kind] || set[repFol, kind] || set[0, kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}

	
	void SL() {
		code = new Code(this); tab = code.tab; Operand.parser = this; 
		int ep = 0; code.PutJMP(ref ep); 
		Expect(4);
		Obj o = tab.insert(Obj.Kind.Prog, "main", Tab.intType);
		tab.openScope();
		
		while (la.kind == 8 || la.kind == 12) {
			Declaration();
		}
		Expect(5);
		code.Fixup(ep); curMeth = o; 
		StatSeq();
		Expect(6);
		Expect(7);
		code.PutRET(0); 
		o.locals = tab.curScope.locals;
		tab.closeScope();
		
		
	}

	void Declaration() {
		if (la.kind == 8) {
			VarDecl();
		} else if (la.kind == 12) {
			ProcDecl();
		} else SynErr(35);
	}

	void StatSeq() {
		Statement();
		while (la.kind == 10) {
			Get();
			Statement();
		}
	}

	void VarDecl() {
		StringList l; Struct type; 
		Expect(8);
		while (la.kind == 1) {
			IdList(out l);
			Expect(9);
			Type(out type);
			Expect(10);
			foreach (string s in l)
			{
			tab.insert(Obj.Kind.Var, s, type);
			}
			
		}
	}

	void ProcDecl() {
		Expect(12);
		Expect(1);
		Obj o = tab.insert(Obj.Kind.Proc, t.val, Tab.noType);
		o.adr = code.pc;
		tab.openScope();
		
		if (la.kind == 13) {
			tab.curScope.localsSize = 8; 
			Parameters(out o.type);
		}
		o.nPars = tab.curScope.locals.Count;
		tab.curScope.paramsSize = tab.curScope.GetLocalsSize();
		tab.curScope.localsSize = 0;
		
		Expect(10);
		while (la.kind == 8) {
			VarDecl();
		}
		o.locals = tab.curScope.locals;
		code.PutMethEntry(tab.curScope.GetLocalsSize());
		
		if (la.kind == 5) {
			Get();
			curMeth = o; 
			StatSeq();
		}
		Expect(6);
		Expect(1);
		Expect(10);
		code.PutMethExit(tab.curScope.paramsSize);
		tab.closeScope();
		
	}

	void IdList(out StringList l) {
		l = new StringList(); 
		Expect(1);
		l.Add(t.val); 
		while (la.kind == 11) {
			Get();
			Expect(1);
			l.Add(t.val); 
		}
	}

	void Type(out Struct s) {
		Expect(1);
		Obj o = tab.find(t.val);
		if (o.kind != Obj.Kind.Type) SemErr("Expected type specifier");
		s = o.type;
		
	}

	void Parameters(out Struct retType) {
		retType = Tab.noType; 
		Expect(13);
		if (la.kind == 1 || la.kind == 8) {
			Param();
			while (la.kind == 10) {
				Get();
				Param();
			}
		}
		Expect(14);
		if (la.kind == 9) {
			Get();
			Type(out retType);
		}
	}

	void Param() {
		StringList l; Struct type; Obj.Kind parKind = Obj.Kind.Var; 
		if (la.kind == 8) {
			Get();
			parKind = Obj.Kind.VarPar; 
		}
		IdList(out l);
		Expect(9);
		Type(out type);
		foreach (string s in l)
		{
		tab.insert(parKind, s, type);
		}
		
	}

	void Statement() {
		Operand x, y; Obj o; 
		int start = 0, els = 0, end = 0; 
		if (StartOf(1)) {
			if (la.kind == 1) {
				Get();
				o = tab.find(t.val); 
				if (la.kind == 15) {
					x = new Operand(o); 
					Get();
					Expression(out y);
					code.Load(y); code.Deref(x); code.PutMOV(x, y); code.FreeAllRegs(); 
					if (!x.type.assignableTo(y.type)) SemErr("Incompatible types for assignment"); 
				} else if (la.kind == 13) {
					if (o.name == "Put" || o.name == "PutLn") code.put(0x57); /*push edi */ 
					ActParameters(o);
					code.PutCALL(o); code.FreeAllRegs(); 
					if (o.name == "Put" || o.name == "PutLn") code.put(0x5f); /*pop edi */ 
				} else SynErr(36);
			} else if (la.kind == 16) {
				Get();
				Condition(out x);
				code.PutJcc(x.op, true, true, ref els); 
				Expect(17);
				StatSeq();
				while (la.kind == 18) {
					Get();
					code.PutJMP(ref end); code.Fixup(els); els = 0; 
					Condition(out x);
					code.PutJcc(x.op, true, true, ref els); 
					Expect(17);
					StatSeq();
				}
				if (la.kind == 19) {
					Get();
					code.PutJMP(ref end); code.Fixup(els); 
					StatSeq();
				} else if (la.kind == 6) {
					code.Fixup(els); 
				} else SynErr(37);
				Expect(6);
				code.Fixup(end); 
			} else if (la.kind == 20) {
				Get();
				start = code.pc; 
				Condition(out x);
				code.PutJcc(x.op, true, true, ref end); 
				Expect(21);
				StatSeq();
				Expect(6);
				code.PutJMP(ref start); code.Fixup(end); 
			} else {
				Get();
				if (StartOf(2)) {
					Expression(out x);
					code.DoReturnPrep(x);  
					if (curMeth.type == Tab.noType) SemErr("Unexpected return value"); 
					if (!x.type.assignableTo(curMeth.type)) SemErr("Invalid type of return value"); 
				} else if (StartOf(3)) {
					if (curMeth.type != Tab.noType) SemErr("Expected return value"); 
				} else SynErr(38);
				code.PutMethExit(tab.curScope.paramsSize); 
			}
		}
	}

	void Expression(out Operand x) {
		Code.ExprOp op = Code.ExprOp.add; 
		if (la.kind == 29 || la.kind == 30) {
			Addop(out op);
		}
		Term(out x);
		code.GenOp(op, x); 
		while (la.kind == 29 || la.kind == 30) {
			Operand y; 
			Addop(out op);
			Term(out y);
			code.GenOp(op, x, y); 
			if (!x.type.compatibleWith(y.type)) SemErr("Incompatible types in expression"); 
		}
	}

	void ActParameters(Obj m) {
		Operand x; Obj o;
		if (m.kind != Obj.Kind.Proc) SemErr("Procedure expected");
		OperandList ops = new OperandList();
		
		Expect(13);
		if (StartOf(2)) {
			Expression(out x);
			ops.Add(x); 
			while (la.kind == 11) {
				Get();
				Expression(out x);
				ops.Add(x); 
			}
		}
		Expect(14);
		if (ops.Count != m.nPars) SemErr("Parameter count does not match");
		for (int q = ops.Count-1; q >= 0; q--) {
		x = ops[q];
		o = m.locals[q].Value;
		if (o.kind == Obj.Kind.Var) code.Load(x);
		else if (o.kind == Obj.Kind.VarPar) { code.PointerOperand(x); code.LoadAdr(x); }
		if (!x.type.assignableTo(o.type)) SemErr("Invalid parameter type");
		code.PutPUSH(x);
		code.FreeOperand(x);
		}
		
		
	}

	void Condition(out Operand x) {
		Operand y; Code.CompOp op; 
		Expression(out x);
		Relop(out op);
		Expression(out y);
		if (!x.type.compatibleWith(y.type)) SemErr("Incompatible types for comparison");
		code.PutDyadic(Code.DyadicOp.cmp, x, y);
		code.FreeOperand(x); code.FreeOperand(y);
		x.kind = Operand.Kind.Cond;
		x.op = op;
		
	}

	void Relop(out Code.CompOp op) {
		op=0; 
		switch (la.kind) {
		case 23: {
			Get();
			op = Code.CompOp.eq; 
			break;
		}
		case 24: {
			Get();
			op = Code.CompOp.neq; 
			break;
		}
		case 25: {
			Get();
			op = Code.CompOp.lt; 
			break;
		}
		case 26: {
			Get();
			op = Code.CompOp.gt; 
			break;
		}
		case 27: {
			Get();
			op = Code.CompOp.geq; 
			break;
		}
		case 28: {
			Get();
			op = Code.CompOp.leq; 
			break;
		}
		default: SynErr(39); break;
		}
	}

	void Addop(out Code.ExprOp op) {
		op=0; 
		if (la.kind == 29) {
			Get();
			op = Code.ExprOp.add; 
		} else if (la.kind == 30) {
			Get();
			op = Code.ExprOp.sub; 
		} else SynErr(40);
	}

	void Term(out Operand x) {
		Code.ExprOp op; Operand y; 
		Factor(out x);
		while (la.kind == 31 || la.kind == 32 || la.kind == 33) {
			Mulop(out op);
			Factor(out y);
			code.GenOp(op, x, y); 
			if (!x.type.compatibleWith(y.type)) SemErr("Incompatible types in expression"); 
		}
	}

	void Factor(out Operand x) {
		x = new Operand(); 
		if (la.kind == 1) {
			Get();
			Obj o = tab.find(t.val); 
			if (o.kind == Obj.Kind.Type) SemErr("Expression expected (found type)"); 
			if (StartOf(4)) {
				x = new Operand(o); 
			} else if (la.kind == 13) {
				bool saveEAX = code.SaveEAX(); 
				ActParameters(o);
				if (o.type == Tab.noType) SemErr("Procedure has no return value"); 
				code.PutCALL(o);
				if (o.name == "ORD" || o.name == "CHR") code.put(0x58); //pop eax
				x = code.RegOperand(Register.Reg.EAX);
				if (saveEAX)
				   {
				Operand result = code.RegOperand(null);
				       code.PutMOV(result, x);
				       code.RestoreEAX();
				x = result;
				   }
				x.type = o.type;
				
			} else SynErr(41);
		} else if (la.kind == 2) {
			Get();
			x.kind = Operand.Kind.Con;
			x.val = Int32.Parse(t.val);
			x.type = Tab.intType; 
		} else if (la.kind == 3) {
			Get();
			x.kind = Operand.Kind.Con;
			x.val = t.val[1];
			x.type = Tab.charType; 
		} else if (la.kind == 13) {
			Get();
			Expression(out x);
			Expect(14);
		} else SynErr(42);
	}

	void Mulop(out Code.ExprOp op) {
		op=0; 
		if (la.kind == 31) {
			Get();
			op = Code.ExprOp.mul; 
		} else if (la.kind == 32) {
			Get();
			op = Code.ExprOp.div; 
		} else if (la.kind == 33) {
			Get();
			op = Code.ExprOp.mod; 
		} else SynErr(43);
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		SL();
		Expect(0);

	}
	
	static readonly bool[,] set = {
		{_T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x},
		{_x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_x,_x,_x, _T,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x},
		{_x,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x},
		{_x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_T,_T, _x,_x,_T,_x, _x,_T,_T,_T, _x,_T,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x}

	};
} // end Parser


public class Errors {
	public int count = 0;                                    // number of errors detected
	public System.IO.TextWriter errorStream = Console.Out;   // error messages go to this stream
	public string errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

	public virtual void SynErr (int line, int col, int n) {
		string s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "ident expected"; break;
			case 2: s = "number expected"; break;
			case 3: s = "charCon expected"; break;
			case 4: s = "\"PROGRAM\" expected"; break;
			case 5: s = "\"BEGIN\" expected"; break;
			case 6: s = "\"END\" expected"; break;
			case 7: s = "\".\" expected"; break;
			case 8: s = "\"VAR\" expected"; break;
			case 9: s = "\":\" expected"; break;
			case 10: s = "\";\" expected"; break;
			case 11: s = "\",\" expected"; break;
			case 12: s = "\"PROCEDURE\" expected"; break;
			case 13: s = "\"(\" expected"; break;
			case 14: s = "\")\" expected"; break;
			case 15: s = "\":=\" expected"; break;
			case 16: s = "\"IF\" expected"; break;
			case 17: s = "\"THEN\" expected"; break;
			case 18: s = "\"ELSIF\" expected"; break;
			case 19: s = "\"ELSE\" expected"; break;
			case 20: s = "\"WHILE\" expected"; break;
			case 21: s = "\"DO\" expected"; break;
			case 22: s = "\"RETURN\" expected"; break;
			case 23: s = "\"=\" expected"; break;
			case 24: s = "\"#\" expected"; break;
			case 25: s = "\"<\" expected"; break;
			case 26: s = "\">\" expected"; break;
			case 27: s = "\">=\" expected"; break;
			case 28: s = "\"<=\" expected"; break;
			case 29: s = "\"+\" expected"; break;
			case 30: s = "\"-\" expected"; break;
			case 31: s = "\"*\" expected"; break;
			case 32: s = "\"/\" expected"; break;
			case 33: s = "\"%\" expected"; break;
			case 34: s = "??? expected"; break;
			case 35: s = "invalid Declaration"; break;
			case 36: s = "invalid Statement"; break;
			case 37: s = "invalid Statement"; break;
			case 38: s = "invalid Statement"; break;
			case 39: s = "invalid Relop"; break;
			case 40: s = "invalid Addop"; break;
			case 41: s = "invalid Factor"; break;
			case 42: s = "invalid Factor"; break;
			case 43: s = "invalid Mulop"; break;

			default: s = "error " + n; break;
		}
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}

	public virtual void SemErr (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}
	
	public virtual void SemErr (string s) {
		errorStream.WriteLine(s);
		count++;
	}
	
	public virtual void Warning (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
	}
	
	public virtual void Warning(string s) {
		errorStream.WriteLine(s);
	}
} // Errors


public class FatalError: Exception {
	public FatalError(string m): base(m) {}
}
}