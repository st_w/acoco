﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SL.symtab
{
    public class Scope
    {
        /** Reference to enclosing scope. */
        public Scope outer { get; private set; }
        /** Declarations of this scope. */
        public KeyedList<String, Obj> locals { get; private set; }
        /** Number of variables in this scope. */
        public int localsSize = 0;
        public int paramsSize = 0;

        public Scope(Scope outer)
        {
            this.outer = outer;
            this.locals = new KeyedList<String, Obj>();
        }

        public Obj findGlobal(String name)
        {
            Obj res = findLocal(name);
            if (res == null && outer != null)
            {
                res = outer.findGlobal(name);
            }
            return res;
        }

        public Obj findLocal(String name)
        {
            return locals.ContainsKey(name) ? locals[name] : null;
        }

        public void insert(Obj o)
        {
            locals.Add(o.name, o);
            if (o.kind == Obj.Kind.Var || o.kind == Obj.Kind.VarPar)
            {
                if (localsSize <= 0) localsSize -= o.type.size;  // locals
                else localsSize += o.type.size == 1 ? 4 : o.type.size;  // parameters
            }
        }

        public int GetLocalsSize()
        {
            if (localsSize <= 0) return -localsSize;  // locals
            else return localsSize - 8;  // parameters
        }


    }
}
