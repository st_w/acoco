﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SL.symtab
{
    public class Struct
    {
        /** Possible codes for structure kinds. */
        public enum Kind
        {
            None, Int, Char, Arr, Record, Pointer
        }

        /** Kind of the structure node. */
        public readonly Kind kind;
        /** Only for Arr: Type of the array elements. */
        public readonly Struct elemType;
        /** Only for Record: fields. */
        public Dictionary<String, Obj> fields = new Dictionary<string, Obj>();

        public int size;

        public Struct(Kind kind, Struct elemType)
        {
            this.kind = kind;
            this.elemType = elemType;
            if (this.kind == Kind.Pointer) this.size = 4;
        }

        public Struct(Kind kind, int sz)
        {
            this.kind = kind;
            this.elemType = null;
            this.size = sz;
        }


        /**
         * Retrieves the field <code>name</code>.
         */
        public Obj findField(String name)
        {
            return fields[name];
        }

        /** Only for Class: Number of fields. */
        public int nrFields()
        {
            return fields.Count;
        }

        // TODO Exercise 5: checks for different kinds of type compatibility

        //public bool isRefType()
        //{
        //    return kind == Kind.Arr;
        //}

        bool equals(Struct other)
        {
            if (kind == Kind.Arr)
            {
                return other.kind == Kind.Arr && elemType.equals(other.elemType);
            }
            else
            {
                return this == other; // must be same type node
            }
        }

        public bool compatibleWith(Struct other)
        {
            if (other.kind != Kind.Pointer && this.kind != Kind.Pointer) return this.equals(other);
            return getPointerType(this) == getPointerType(other);
        }

        public bool assignableTo(Struct dest)
        {
            if (dest.kind != Kind.Pointer && this.kind != Kind.Pointer) return this.equals(dest);
            return getPointerType(this) == getPointerType(dest);
        }

        Struct getPointerType(Struct t)
        {
            Struct i = t;
            while (i.kind == Kind.Pointer) i = i.elemType;
            return i;
        }

    }
}
