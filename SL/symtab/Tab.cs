﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SL.symtab
{
    public class Tab
    {
        // Universe
        public static readonly Struct noType = new Struct(Struct.Kind.None, 0);
        public static readonly Struct intType = new Struct(Struct.Kind.Int, 4);
        public static readonly Struct charType = new Struct(Struct.Kind.Char, 1);

        public Obj noObj, chrObj, ordObj, lenObj;

        /** Only used for reporting errors. */
        private readonly Parser parser;
        /** The current top scope. */
        public Scope curScope;
        /** Nesting level of current scope. */
        public int curLevel { get; private set; }

        // TODO Exercise 4: implementation of symbol table

        /**
         * Set up "universe" (= predefined names).
         */
        public Tab(Parser p)
        {
            parser = p;
            curScope = new Scope(null);
            curLevel = 0;

            // dummy object
            noObj = new Obj(Obj.Kind.Con, "!!!noObj!!!", noType);

            // set up "universe" (= predefined names)
            insert(Obj.Kind.Type, "INTEGER", intType);
            insert(Obj.Kind.Type, "CHAR", charType);
		
            chrObj = insert(Obj.Kind.Proc, "CHR", charType);
            openScope();
            insert(Obj.Kind.Var, "i", intType);
            chrObj.locals = curScope.locals;
            chrObj.nPars = 1;
            closeScope();

            ordObj = insert(Obj.Kind.Proc, "ORD", intType);
            openScope();
            insert(Obj.Kind.Var, "ch", charType);
            ordObj.locals = curScope.locals;
            ordObj.nPars = 1;
            closeScope();

            ordObj = insert(Obj.Kind.Proc, "Put", noType);
            openScope();
            insert(Obj.Kind.Var, "e", charType);
            ordObj.locals = curScope.locals;
            ordObj.nPars = 1;
            closeScope();

            ordObj = insert(Obj.Kind.Proc, "PutLn", noType);

            curLevel = -1;
        }

        public void openScope()
        {
            Scope s = new Scope(curScope);
            curScope = s;
            curLevel++;
        }

        public void closeScope()
        {
            curScope = curScope.outer;
            curLevel--;
        }

        public Obj insert(Obj.Kind kind, String name, Struct type)
        {
            Obj o = new Obj(kind, name, type);
            if (curScope.findLocal(name) != null)
            {
                parser.SemErr("DUPLICATE DECL: " + name);
                return noObj;
            }
            if (o.kind == Obj.Kind.Var || o.kind == Obj.Kind.VarPar)
            {
                o.level = curLevel;
                o.adr = curScope.localsSize;
                if (curScope.localsSize <= 0) o.adr -= o.type.size;     // address correction for locals
            }
            curScope.insert(o);
            return o;
        }

        //may return null and does not throw errors
        public Obj tryFind(String name)
        {
            Obj o = curScope.findGlobal(name);
            return o;
        }

        //never returns null (noObj instead) and throws NOT_FOUND error in that case
        public Obj find(String name)
        {
            Obj o = tryFind(name);
            if (o == null)
            {
                parser.SemErr("NOT_FOUND: " + name);
                o = noObj;
            }
            return o;
        }

        public Obj findField(String name, Struct type)
        {
            Obj o = type.findField(name);
            if (o == null)
            {
                parser.SemErr("NO_FIELD: " + name);
                o = noObj;
            }
            return o;
        }


    }
}
