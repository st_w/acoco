﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SL.symtab
{
    public class Obj
    {

        /** Possible codes for object kinds. */
        public enum Kind
        {
            Con, Var, VarPar, Type, Proc, Field, Prog
        }

        /** Kind of the object node. */
        public readonly Kind kind;
        /** Name of the object node. */
        public readonly String name;
        /** Type of the object node. */
        public Struct type;
        /** Only for Con: Value of the constant. */
        public int val;
        /** Only for Var, Meth, Field of Record: Offset of the element. */
        public int adr;
        /** Only for Var: Declaration level (0..global, 1..local) */
        public int level;
        /** Only for Meth: Number of parameters. */
        public int nPars;
        /** Only for Meth / Prog: List of local variables / global declarations. */
        public KeyedList<String, Obj> locals = new KeyedList<string, Obj>();

        public Obj(Kind kind, String name, Struct type)
        {
            this.kind = kind;
            this.name = name;
            this.type = type;
        }


    }
}
