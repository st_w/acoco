PROGRAM

  VAR i,j: INTEGER;
  VAR c: CHAR;
  
  PROCEDURE TestMeth (x: INTEGER; y, z, w: CHAR; VAR pi: INTEGER; VAR pc: CHAR) : INTEGER;
    VAR c0, c1, c2, c3: CHAR;
	VAR i1: INTEGER;
  BEGIN
	x := x + 1;
	y := y + '2' + y - y - '1';
	z := z + '5';
	pi := 49374;  /* 0xC0DE */
	i1 := pi;
	pc := 'A';
	c0 := pc;
	RETURN 3;
  END TestMeth;
  
  BEGIN
    i := 1;
	c := '0';
	j := TestMeth (10, 'A', CHR(0), CHR(65), i, c);
	c := CHR(0) + CHR(16);
    PutLn();
  END.

