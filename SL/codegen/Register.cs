﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SL.codegen
{
    public class Register
    {

        private List<Reg> usedRegs = new List<Reg>();

        public enum Reg
        {
            EAX = 0, EBX = 3, ECX = 1, EDX = 2, ESP = 4, EBP = 5, EDI = 7, ESI = 6
        }

        Reg? tryGetReg(Reg r)
        {
            if (!usedRegs.Contains(r))
            {
                usedRegs.Add(r);
                return r;
            }
            else
            {
                return null;
            }
        }

        // yields an arbitrary free register and marks it as used
        // search order: EBX, EDX, ECX, EAX, EDI, ESI
        public Reg? GetReg(Reg? r = null)
        {
            if (r != null && usedRegs.Contains(r.Value)) throw new Exception("Register already in use");
            if (r != null) return tryGetReg(r.Value).Value;
            return tryGetReg(Reg.EBX) ?? tryGetReg(Reg.EDX) ?? tryGetReg(Reg.ECX) ?? tryGetReg(Reg.ESI) ?? tryGetReg(Reg.EAX) ?? null;
        }

        // FreeReg(r); marks register r as free
        public void FreeReg(Reg? r)
        {
            if (r.HasValue)
                usedRegs.Remove(r.Value);
        }

        // IsFree(r) returns true, if register r is free
        public bool IsFree(Reg r)
        {
            return !usedRegs.Contains(r);
        }

        // marks all registers as free (e.g. at the end of a statement)
        public void FreeAllRegs()
        {
            usedRegs.Clear();
        }



    }
}
