﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SL.symtab;

namespace SL.codegen
{
    public class Operand
    {
        /** Possible operands. */
        public enum Kind
        {
            Con, Reg, RegRel, Abs, Proc, Cond
        }

        /** Kind of the operand. */
        public Kind kind;
        /** The type of the operand (reference to symbol table). */
        public Struct _type;
        /** Only for Con: Value of the constant. */
        public int val;
        // only for Reg, RegRel: register
        public Register.Reg? reg;
        /** Only for Abs, RegRel, Meth: Offset of the element. */
        public int adr;
        // only for Abs, RegRel
        public Register.Reg? inx;
        // 0 for no scaling, 1 for 2*scaling, 2 for 4*scaling
        public int scale;

        // pointer dereference count;
        public int deref = 0;

        /** Only for Cond: Relational operator. */
        public Code.CompOp op;
        /** Only for Meth: Method object from the symbol table. */
        public Obj obj;
        ///** Only for Cond: Target for true jumps. */
        //public int tLabel;
        ///** Only for Cond: Target for false jumps. */
        //public int fLabel;

        public Struct type
        {
            get
            {
                if (_type == null && kind == Kind.Reg) return Tab.intType;
                return _type;
            }
            set { _type = value; }
        }

        public static Parser parser = null;

        // Constructor for named objects: constants, variables, methods
        public Operand(Obj o)
        {
            type = o.type;
            val = o.val;
            adr = o.adr;
            switch (o.kind)
            {
                case Obj.Kind.Con:
                    kind = Kind.Con;
                    break;
                case Obj.Kind.Var:
                    if (o.level == 0)
                    {
                        kind = Kind.RegRel;
                        reg = Register.Reg.EDI;
                        adr = (-o.adr-o.type.size)+8;
                    }
                    else
                    {
                        kind = Kind.RegRel;
                        reg = Register.Reg.EBP;
                        adr = o.adr;

                    }
                    break;
                case Obj.Kind.VarPar:
                    kind = Kind.RegRel;
                    reg = Register.Reg.EBP;
                    adr = o.adr;
                    deref = 1;
                    type = new Struct(Struct.Kind.Pointer, o.type);
                    break;
                case Obj.Kind.Proc:
                    kind = Kind.Proc;
                    obj = o;
                    break;
                default:
                    parser.SemErr("NO_OPERAND");
                    break;
            }
        }

        ///** Constructor for compare operations */
        //public Operand(Code.CompOp op, Code code)
        //    : this(code)
        //{
        //    this.kind = Kind.Cond;
        //    this.op = op;
        //}

        //public Operand(Code code)
        //{
        //    tLabel = new Label(code);
        //    fLabel = new Label(code);
        //}

        ///** Constructor for stack operands */
        //public Operand(Struct type)
        //{
        //    this.kind = Kind.Stack;
        //    this.type = type;
        //}

        /** Constructor for integer constants */
        public Operand(int x)
        {
            kind = Kind.Con;
            type = Tab.intType;
            val = x;
        }

        public Operand()
        {

        }


    }
}
