﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SL.symtab;
using System.Diagnostics;
using System.IO;

namespace SL.codegen
{
    public class Code
    {
        /*
        	public static enum OpCode {
		load, load_0, load_1, load_2, load_3, store, store_0, store_1, store_2,
		store_3, getstatic, putstatic, getfield, putfield, const_0, const_1,
		const_2, const_3, const_4, const_5, const_m1, const_, add, sub, mul,
		div, rem, neg, shl, shr, inc, new_, newarray, aload, astore, baload,
		bastore, arraylength, pop, dup, dup2, jmp, jeq, jne, jlt, jle, jgt,
		jge, call, return_, enter, exit, read, print, bread, bprint, trap, nop;

		public int code() {
			return ordinal() + 1;
		}

		public String cleanName() {
			String name = name();
			if (name.endsWith("_")) {
				name = name.substring(0, name.length() - 1);
			}
			return name;
		}

		public static OpCode get(int code) {
			if (code < 1 || code > values().length) {
				return null;
			}
			return values()[code - 1];
		}
	}
        */

        public enum CompOp
        {
            eq = 0, neq = 1, lt = 2, geq = 3, gt = 4, leq = 5
        }

        public enum DyadicOp
        {
            add = 0x00, adc = 0x10, sub = 0x28, sbb = 0x18, and = 0x20, or = 0x08, xor = 0x30, cmp = 0x38
        }

        public enum ExprOp
        {
            add, sub, mul, div, mod
        }



        //public static CompOp invert(CompOp op)
        //{
        //    switch (op)
        //    {
        //        case CompOp.eq:
        //            return CompOp.ne;
        //        case CompOp.ne:
        //            return CompOp.eq;
        //        case CompOp.lt:
        //            return CompOp.ge;
        //        case CompOp.le:
        //            return CompOp.gt;
        //        case CompOp.gt:
        //            return CompOp.le;
        //        case CompOp.ge:
        //            return CompOp.lt;
        //    }
        //    throw new Exception("Unexpected CompOp");
        //}



        /** Code buffer */
        public byte[] buf;

        /**
         * Program counter. Indicates next free byte in code buffer.
         */
        public int pc;

        /** PC of main method (set by parser). */
        public int mainpc;

        /** Length of static data in words (set by parser). */
        public int dataSize;

        /** According parser. */
        private Parser parser;


        private Register regs = new Register();


        public Tab tab { get; private set; }


        // ----- initialization

        public Code(Parser p)
        {
            parser = p;
            buf = new byte[3000];
            pc = 0;
            mainpc = -1;
            dataSize = 0;

            this.tab = new Tab(parser);
        }

        // ----- code storage management

        //public void put(OpCode code)
        //{
        //    put(code.code());
        //}

        public void put(int x)
        {
            if (pc == buf.Length)
            {
                Array.Resize<byte>(ref buf, buf.Length * 2);
            }
            buf[pc++] = (byte)x;
        }

        public void put2(int x)
        {
            put(x);
            put(x >> 8);
        }

        public void put4(int x)
        {
            put2(x);
            put2(x >> 16);
        }

        public void put2(int pos, int x)
        {
            int oldpc = pc;
            pc = pos;
            put2(x);
            pc = oldpc;
        }

        public void put4(int pos, int x)
        {
            int oldpc = pc;
            pc = pos;
            put4(x);
            pc = oldpc;
        }

        public int get(int pos)
        {
            return buf[pos];
        }

        public int get2(int pos)
        {
            Debug.Assert(BitConverter.IsLittleEndian);
            return BitConverter.ToInt16(buf, pos);
        }

        public int get4(int pos)
        {
            Debug.Assert(BitConverter.IsLittleEndian);
            return BitConverter.ToInt32(buf, pos);
        }

        // TODO Exercise 5 - 6: implementation of code generation



        public Operand RegOperand(Register.Reg? reg)
        {
            Operand x = new Operand();
            x.kind = Operand.Kind.Reg; x.type = Tab.intType; x.adr = 0; x.inx = null;
            if (reg == null) x.reg = regs.GetReg(); else x.reg = regs.GetReg(reg);
            if (x.reg == null) throw new Exception("OUT_OF_REGS no more free register available");
            return x;
        }

        public void FreeOperand(Operand r)
        { // deallocate all registers held by r
            if (r.kind == Operand.Kind.Reg || r.kind == Operand.Kind.RegRel)
            {
                regs.FreeReg(r.reg); r.reg = null;
            }
            if (r.inx != null)
            {
                regs.FreeReg(r.inx); r.inx = null;
            }
        }

        Operand RegRelOperand(Register.Reg reg, int adr)
        {
            Operand x = new Operand();
            x.kind = Operand.Kind.RegRel; x.reg = reg; x.adr = adr; x.inx = null;
            return x;
        }

        public void PointerOperand(Operand x)
        {
            x.deref++;
            x.type = new Struct(Struct.Kind.Pointer, x.type);
        }


        // --------------------------------------------


        public void FreeAllRegs()
        {
            regs.FreeAllRegs();
        }

        public bool SaveEAX()
        {
            if (regs.IsFree(Register.Reg.EAX)) return false;
            put(0x50); // push eax
            regs.FreeReg(Register.Reg.EAX);
            return true;
        }

        public void RestoreEAX() {
            if (regs.IsFree(Register.Reg.EAX)) regs.GetReg(Register.Reg.EAX);
            put(0x58); // pop eax
        }


        static int Mod(int n)
        {
            if (n == 0) return 0;
            else if (n >= -128 && n < 127) return 1;
            else return 2;
        }

        void PutOperands(int reg, Operand x)
        {
            //----- put modrm byte
            int mod = 0, rm = 0;
            if (x.inx == null)
            {
                switch (x.kind)
                {
                    case Operand.Kind.Abs:
                        mod = 0; rm = 5; break;
                    case Operand.Kind.Reg:
                        mod = 3; rm = (int)x.reg; break;
                    case Operand.Kind.RegRel:
                        mod = Mod(x.adr); rm = (int)x.reg;
                        if (mod == 0 && rm == 5) { mod = 1; x.adr = 0; }  // encode [EBP] as [EBP+0x00], as the other is used for [imm]
                        break;
                }
            }
            else
            { // indexed
                Debug.Assert(x.inx != Register.Reg.ESP && (x.kind == Operand.Kind.Abs || x.kind == Operand.Kind.RegRel));
                rm = 4;
                if (x.kind == Operand.Kind.Abs)
                {
                    mod = 0; x.reg = Register.Reg.EBP;
                }
                else
                {
                    mod = Mod(x.adr);
                }
            }
            put((mod << 6) + ((int)reg << 3) + rm);

            //----- put SIB byte
            if (x.inx != null) put((x.scale << 6) + ((int)x.inx << 3) + (int)x.reg);
            if (x.reg == Register.Reg.ESP && x.kind == Operand.Kind.RegRel) put((4 << 3) + (int)x.reg);     //no scaling

            //----- put displacement
            if (mod == 0 && rm == 5) PutConst(4, x.adr); // absolute address
            else if (mod == 0 && rm == 4 && x.reg == Register.Reg.EBP) PutConst(4, x.adr); // absolute indexed
            else if (mod == 1) PutConst(1, x.adr);
            else if (mod == 2) PutConst(4, x.adr);

        }

        void PutConst(int sz, int addr)
        {
            switch (sz)
            {
                case 1: put(addr); break;
                case 2: put2(addr); break;
                case 4: put4(addr); break;
                default: parser.SemErr("Error putting constant"); break;
            }
        }

        void PutConst(Operand x)
        {
            Debug.Assert(x.kind == Operand.Kind.Con);
            PutConst(x.type.size, x.val);
        }

        public void PutMOV(Operand x, Operand y)
        { // MOV x, y
            Debug.Assert(x.type.size == y.type.size);
            int sizeFlag = PutPrefix(x);
            if (y.kind == Operand.Kind.Con)
            {
                if (x.kind == Operand.Kind.Reg)
                { // r := imm
                    put(0xB0 + (sizeFlag << 3) + (int)x.reg);
                    PutConst(y.type.size, y.val);
                }
                else
                { // m := imm
                    put(0xC6 + sizeFlag); PutOperands(0, x);
                    PutConst(y.type.size, y.val);
                }
            }
            else if (x.kind == Operand.Kind.Reg)
            { // r := rm
                put(0x8A + sizeFlag); PutOperands((int)x.reg, y);
            }
            else if (y.kind == Operand.Kind.Reg)
            { // rm := r
                put(0x88 + sizeFlag); PutOperands((int)y.reg, x);
            }
            else parser.SemErr("Error putting MOV");
        }


        int PutPrefix(Operand x)
        {
            //if (x.type == null) return 1;       // assume 4 bytes when no type is known (register)
            if (x.type.size == 1) return 0;
            else if (x.type.size == 2) { put(0x66); return 1; }
            else if (x.type.size == 4) return 1;
            else { parser.SemErr("Error putting prefix"); return 0; }
        }


        public void PutDyadic(DyadicOp op, Operand x, Operand y)
        {
            int sf = PutPrefix(x);
            if (x.kind == Operand.Kind.Reg && x.reg == Register.Reg.EAX && y.kind == Operand.Kind.Con && x.type.size == y.type.size)
            {
                // EAX := EAX op imm
                put((int)op + 4 + sf); PutConst(y.type.size, y.val);
            }
            else if (y.kind == Operand.Kind.Con)
            {
                if (x.type.size > 1 && -128 <= y.val && y.val <= 127)
                { // rm := rm op signextend(imm8)
                    put(0x82 + sf); PutOperands((int)op / 8, x); PutConst(1, y.val);
                }
                else if (x.type.size == y.type.size)
                { // rm := rm op imm
                    put(0x80 + sf); PutOperands((int)op / 8, x); PutConst(y.type.size, y.val);
                }
                else parser.SemErr("Error putting dyadic");
            }
            else if (x.kind == Operand.Kind.Reg)
            { // r := r op rm
                put((int)op + 2 + sf); PutOperands((int)x.reg, y);
            }
            else if (y.kind == Operand.Kind.Reg)
            { // rm := rm op r
                put((int)op + sf); PutOperands((int)y.reg, x);
            }
            else parser.SemErr("Error putting dyadic");
            FreeOperand(y);
        }

        public void GenOp(ExprOp op, Operand x)
        {
            if (x.kind == Operand.Kind.Con)
            {
                switch (op)
                {
                    case ExprOp.add: break;
                    case ExprOp.sub: x.val = -x.val; break;
                }
            }
            else
            {
                switch (op)
                {
                    case ExprOp.add: break;
                    case ExprOp.sub: Load(x); PutNEG(x); break;
                }
            }
        }

        public void GenOp(ExprOp op, Operand x, Operand y)
        {
            if (x.kind == Operand.Kind.Con && y.kind == Operand.Kind.Con)
            {
                switch (op)
                {
                    case ExprOp.add: x.val += y.val; break;
                    case ExprOp.sub: x.val -= y.val; break;
                    case ExprOp.mul: x.val *= y.val; break;
                    case ExprOp.div: x.val /= y.val; break;
                    case ExprOp.mod: x.val %= y.val; break;
                }
            }
            else
            {
                Load(x);
                switch (op)
                {
                    case ExprOp.add: PutDyadic(DyadicOp.add, x, y); break;
                    case ExprOp.sub: PutDyadic(DyadicOp.sub, x, y); break;
                    case ExprOp.mul: PutMUL(x, y); break;
                    case ExprOp.div: PutDIV(x, y); break;
                    case ExprOp.mod: PutDIV(x, y, true); break;
                }
            }
        }


        public void PutLEA(Operand x, Operand y)
        {
            int sf = PutPrefix(x);
            put(0x8D);
            if (x.kind != Operand.Kind.Reg) parser.SemErr("Error putting LEA: dest has to be register");
            PutOperands((int)x.reg, y);
        }

        public void PutMUL(Operand x, Operand y)
        {
            //EAX multiplied by reg/mem -> EAX:EDX

            Operand r = new Operand();
            r.kind = Operand.Kind.Reg;

            bool saveEAX = !regs.IsFree(Register.Reg.EAX);
            bool saveEDX = !regs.IsFree(Register.Reg.EDX);
            bool saveECX = !regs.IsFree(Register.Reg.ECX);

            if (saveEAX) put(0x50); //push eax
            if (saveEDX) put(0x52); //push edx
            if (saveECX) put(0x51); //push ecx

            r.reg = Register.Reg.ECX; PutMOV(r, y);
            r.reg = Register.Reg.EAX; PutMOV(r, x);

            put(0xf7); put(0xe1);   // mul ecx

            r.reg = Register.Reg.EAX;
            Operand ret = RegOperand(null);
            PutMOV(ret, r);
            x.reg = ret.reg;
            x.kind = ret.kind;

            if (saveECX) put(0x59); //pop ecx
            if (saveEDX) put(0x5A); //pop edx
            if (saveEAX) put(0x58); //pop eax
        }

        public void PutDIV(Operand x, Operand y, bool mod = false)
        {
            //EDX:EAX divided by reg/mem -> EAX, rem: EDX

            Operand r = new Operand();
            r.kind = Operand.Kind.Reg;
            
            bool saveEAX = !regs.IsFree(Register.Reg.EAX);
            bool saveEDX = !regs.IsFree(Register.Reg.EDX);
            bool saveECX = !regs.IsFree(Register.Reg.ECX);

            if (saveEAX) put(0x50); //push eax
            if (saveEDX) put(0x52); //push edx
            if (saveECX) put(0x51); //push ecx

            put(0x31); put(0xD2);  //xor edx, edx
            r.reg = Register.Reg.ECX; PutMOV(r, y);
            r.reg = Register.Reg.EAX; PutMOV(r, x);

            put(0xf7); put(0xf1);   // div ecx

            r.reg = Register.Reg.EAX;
            if (mod) r.reg = Register.Reg.EDX;
            Operand ret = RegOperand(null);
            PutMOV(ret, r);
            x.reg = ret.reg;
            x.kind = ret.kind;
  
            if (saveECX) put(0x59); //pop ecx
            if (saveEDX) put(0x5A); //pop edx
            if (saveEAX) put(0x58); //pop eax
            
        }

        public void PutNEG(Operand x)
        {
            int sf = PutPrefix(x);
            put(0xF6 + sf);
            PutOperands(3, x);
        }



        public void PutMethEntry(int n = 0)
        {
            // push ebp
            put(0x55);

            // mov ebp, esp
            put(0x89); put(0xE5);

            // sub esp, n
            if (n > 0)
            {
                Operand x = new Operand();
                x.kind = Operand.Kind.Reg;
                x.reg = Register.Reg.ESP;
                Operand y = new Operand();
                y.kind = Operand.Kind.Con;
                y.type = Tab.intType;
                y.val = n;
                PutDyadic(DyadicOp.sub, x, y);
            }
        }

        public void PutMethExit(int n = 0)
        {
            // mov esp, ebp
            put(0x89); put(0xEC);
            // pop ebp
            put(0x5d);

            PutRET(n);
        }


        public void PutRET(int n = 0)
        {
            // near ret
            put(0xc2);
            put2(n);
        }

        public void DoReturnPrep(Operand x)
        {
            Load(x);
            if (x.kind == Operand.Kind.Reg && x.reg == Register.Reg.EAX) return;
            Operand eax = new Operand();
            eax.kind = Operand.Kind.Reg;
            eax.type = x.type;
            eax.reg = Register.Reg.EAX;
            PutMOV(eax, x);
        }


        public void PutCALL(Obj proc)
        { // prerequisite: parameters are already on the stack
            int addr = proc.adr - (pc + 5);
            if (proc.name == "Put")
            {
                //call [edi]
                put(0xFF); put(0x17);
            }
            else if (proc.name == "PutLn")
            {
                //call [edi+4]
                put(0xFF); put(0x57); put(0x04);
            }
            else if (proc.name == "CHR" || proc.name == "ORD")
            {
                // do nothing
            }
            else
            {
                // near call (relative)
                put(0xe8);
                put4(addr);
            }
        }

        public void PutPUSH(Operand x)
        {
            //FF /6 PUSH r/m16 Push r/m16
            //FF /6 PUSH r/m32 Push r/m32
            //50+rw PUSH r16 Push r16
            //50+rd PUSH r32 Push r32
            //6A PUSH imm8 Push imm8
            //68 PUSH imm16 Push imm16
            //68 PUSH imm32 Push imm32

            int sf = PutPrefix(x);
            if (x.kind == Operand.Kind.Con)
            {
                put(sf==0?0x6A:0x68);
                PutConst(x);
            }
            else if (x.kind == Operand.Kind.Reg)
            {
                //PUSH byte is not supported on x86
                if (x.type.size == 1)
                {
                    Operand tmp = RegOperand(null);
                    //MOVZX tmp, x
                    put(0x0f); put(0xb6);
                    PutOperands((int)tmp.reg, x);
                    //PUSH tmp
                    put(0x50 + (int)tmp.reg);
                    FreeOperand(tmp);

                }
                else
                {
                    put(0x50 + (int)x.reg);

                }

            }
            else
            {
                //PUSH byte is not supported on x86
                if (x.type.size == 1)
                {
                    Operand tmp = RegOperand(null);
                    //MOVZX tmp, x
                    put(0x0f); put(0xb6);
                    PutOperands((int)tmp.reg, x);
                    //PUSH tmp
                    put(0x50 + (int)tmp.reg);
                    FreeOperand(tmp);

                }
                else
                {
                    put(0xFF);
                    PutOperands(6, x);
                }
            }
        }


        public void Load(Operand x, bool deref = true)
        {
            if (deref) Deref(x);
            Operand r = null; // destination register
            if (x.kind == Operand.Kind.Reg)
            {
                if (x.reg == Register.Reg.EDI)  //do not use EDI
                {
                    r = RegOperand(null);
                    r.type = x.type; PutMOV(r, x);
                }
                else return;
            }
            if (x.kind == Operand.Kind.RegRel)
            {
                if (x.reg == Register.Reg.EBP || x.reg == Register.Reg.EDI)  //do not use EDI
                    r = RegOperand(null);
                else
                {
                    if (!regs.IsFree(x.reg.Value)) regs.FreeReg(x.reg);  // FIXME: hack for reusing register allocated during pointer dereference
                    r = RegOperand(x.reg);
                }
                r.type = x.type; PutMOV(r, x); regs.FreeReg(x.inx);
            }
            else if (x.kind == Operand.Kind.Con || x.kind == Operand.Kind.Abs)
            {
                r = RegOperand(null); r.type = x.type;
                PutMOV(r, x); regs.FreeReg(x.inx);
            }
            else parser.SemErr("Error putting LoadOpd");
            x.kind = Operand.Kind.Reg; x.reg = r.reg; x.adr = 0; x.inx = null;
        }


        public void LoadAdr(Operand x)
        {
            if (x.kind == Operand.Kind.RegRel && x.adr == 0 && x.inx == null)
            {
                x.kind = Operand.Kind.Reg;
            }
            else if (x.kind == Operand.Kind.RegRel || x.kind == Operand.Kind.Abs)
            {
                Operand r;
                if (x.kind == Operand.Kind.Abs || x.reg == Register.Reg.EBP || x.reg == Register.Reg.EDI)  //do not use EDI
                    r = RegOperand(null);
                else // x.kind == Operand.RegRel && x.reg != EBP
                    r = RegOperand(x.reg);
                PutLEA(r, x); regs.FreeReg(x.inx);
                x.kind = Operand.Kind.Reg; x.reg = r.reg; x.adr = 0; x.inx = null;
            }
            else parser.SemErr("Error putting LoadAdr"); // Reg, Con, Proc
        }


        public void Deref(Operand x)
        {
            if (x.deref == 0) return;
            x.deref--;
            if (x.kind == Operand.Kind.Reg)
            {
                x.kind = Operand.Kind.RegRel;
            }
            else if (x.kind == Operand.Kind.Con)
            {
                x.kind = Operand.Kind.Abs;
                x.adr = x.val;
            }
            else if (x.kind == Operand.Kind.RegRel || x.kind == Operand.Kind.Abs)
            {
                Load(x, false);
                Debug.Assert(x.kind == Operand.Kind.Reg);
                x.kind = Operand.Kind.RegRel;
            }
            else parser.SemErr("Error: cannot dereference"); // Proc
            x.type = x.type.elemType;
        }



        /*
    //load onto stack
    public void load(Operand x) {
        switch (x.kind) {
        case Con: loadConst(x.val); break;
        case Local:
        switch (x.adr) {
            case 0: put(OpCode.load_0); break;
            case 1: put(OpCode.load_1); break;
            case 2: put(OpCode.load_2); break;
            case 3: put(OpCode.load_3); break;
            default: put(OpCode.load); put(x.adr); break;
        }
        break;
        case Static: put(OpCode.getstatic); put2(x.adr); break;
        case Stack: break; // nothing to do (already loaded)
        case Fld: put(OpCode.getfield); put2(x.adr); break;
        case Elem:
            if (x.type == Tab.charType) { put(OpCode.baload); }
            else { put(OpCode.aload); }
            break;
        default: parser.error(Message.NO_VAL);
        }
        x.kind = Operand.Kind.Stack;
    }
	
    //store from stack
    public void assign(Operand x, Operand y) {
        load(y);
        switch (x.kind) {
        case Local:
            switch (x.adr) {
            case 0: put(OpCode.store_0); break;
            case 1: put(OpCode.store_1); break;
            case 2: put(OpCode.store_2); break;
            case 3: put(OpCode.store_3); break;
            default: put(OpCode.store); put(x.adr); break;
            } break;
        case Static: put(OpCode.putstatic); put2(x.adr); break;
        case Fld: put(OpCode.putfield); put2(x.adr); break;
        case Elem:
            if (x.type == Tab.charType) { put(OpCode.bastore); }
            else { put(OpCode.astore); }
            break;
        default: parser.error(Message.NO_VAR);
        }
    }
	
    private void loadConst(int val) {
        switch (val) {
        case -1: put(OpCode.const_m1); break;
        case 0: put(OpCode.const_0); break;
        case 1: put(OpCode.const_1); break;
        case 2: put(OpCode.const_2); break;
        case 3: put(OpCode.const_3); break;
        case 4: put(OpCode.const_4); break;
        case 5: put(OpCode.const_5); break;
        default: put(OpCode.const_); put4(val); break;
        }
    }
	 */


        public void PutJMP(ref int adr)
        {
            if (adr > 0)
            { // backward jump
                int d = (pc + 2) - adr;
                if (d <= 127)
                {
                    put(0xEB); PutConst(1, -d);
                }
                else
                {
                    put(0xE9); PutConst(4, -(d + 3));
                }
            }
            else
            { // forward jump
                put(0xE9); PutConst(4, adr);
                adr = -(pc - 4);
            }
        }


        public void Fixup(int adr)
        {
            while (adr < 0)
            {
                adr = -adr;
                int x = get4(adr);
                put4(adr, pc - (adr + 4));
                adr = x;
            }
        }

        // For conditional Jumps
        byte[] jccTab = new byte[] {
            0x74, // JE
            0x75, // JNE
            0x7C, // JL
            0x7D, // JGE
            0x7F, // JG
            0x7E, // JLE
            0x74, // JZ
            0x75, // JNZ
            0x72, // JB
            0x73, // JAE
            0x77, // JA
            0x76 // JBE
        };

        public void PutJcc(CompOp cmpOp, bool neg, bool signed, ref int adr)
        {
            int op = (int)cmpOp;
            if (signed) op = jccTab[op]; else op = jccTab[op + 6];
            if (neg) op = op ^ 1;
            if (adr > 0)
            { // backward jump
                int d = (pc + 2) - adr;
                if (d <= 127)
                {
                    put(op); PutConst(1, -d);
                }
                else
                {
                    put(0x0F); put(op + 0x10); PutConst(4, -(d + 4));
                }
            }
            else
            { // forward jump
                put(0x0F); put(op + 0x10); PutConst(4, adr);
                adr = -(pc - 4);
            }
        }



        //public void tJump (Operand x) {
        //    jump(x.op);
        //    x.tLabel.put();
        //}

        //public void fJump (Operand x) {
        //    jump(invert(x.op));
        //    x.fLabel.put();
        //}

        //private void jump(CompOp o) {
        //    switch (o) {
        //    case CompOp.eq: put(OpCode.jeq); break;
        //    case CompOp.ge: put(OpCode.jge); break;
        //    case CompOp.gt: put(OpCode.jgt); break;
        //    case CompOp.le: put(OpCode.jle); break;
        //    case CompOp.lt: put(OpCode.jlt); break;
        //    case CompOp.ne: put(OpCode.jne); break;
        //    }
        //}

        //public void jump(Label l) {
        //    put(OpCode.jmp);
        //    l.put();
        //}


        /**
         * Write the code buffer to the output stream.
         */
        public void write(Stream s)
        {
            System.IO.BinaryWriter os = new System.IO.BinaryWriter(s);
            int codeSize = pc;

            os.Write(this.buf, 0, codeSize);
            os.Flush();
            os.Close();
        }


    }
}
