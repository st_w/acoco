PROGRAM

  VAR i, x: INTEGER;
  VAR c0, c1, c2, c3: CHAR;
  
  BEGIN
    i := 1;
    WHILE i < 100 DO
      i := i + 2
    END;
	IF c0 > '0' THEN x := x / 10;
    ELSIF c1 > '0' THEN x := x % 10;
    ELSIF c2 > '0' THEN x := x * 10;
	ELSE x := x / 10;
    END;
  END.

