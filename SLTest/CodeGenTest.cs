﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SL.codegen;
using SL;
using SL.symtab;

namespace SLTest
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für UnitTest1
    /// </summary>
    [TestClass]
    public class CodeGenTest
    {
        private Code c;

        public CodeGenTest()
        {
            Parser p = new Parser(null);
            c = new Code(p);
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Ruft den Textkontext mit Informationen über
        ///den aktuellen Testlauf sowie Funktionalität für diesen auf oder legt diese fest.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Zusätzliche Testattribute
        //
        // Sie können beim Schreiben der Tests folgende zusätzliche Attribute verwenden:
        //
        // Verwenden Sie ClassInitialize, um vor Ausführung des ersten Tests in der Klasse Code auszuführen.
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Verwenden Sie ClassCleanup, um nach Ausführung aller Tests in einer Klasse Code auszuführen.
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Mit TestCleanup können Sie nach jedem einzelnen Test Code ausführen.
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestLEA()
        {
            //lea    ebx,[ebp+esi*4+0x7b] 
            byte[] expected = new byte[] { 0x8D, 0x5C, 0xB5, 0x7B };
            
            Operand x = new Operand();
            x.reg = Register.Reg.EBX;
            x.kind = Operand.Kind.Reg;
            Operand y = new Operand();
            y.kind = Operand.Kind.RegRel;
            y.inx = Register.Reg.ESI;
            y.reg = Register.Reg.EBP;
            y.scale = 2;
            y.adr = 123;
            c.PutLEA(x, y);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }


        [TestMethod]
        public void TestLEA_EBP()
        {
            //lea    ebx,[ebp]
            byte[] expected = new byte[] { 0x8D, 0x5D, 0x00 };

            Operand x = new Operand();
            x.reg = Register.Reg.EBX;
            x.kind = Operand.Kind.Reg;
            Operand y = new Operand();
            y.kind = Operand.Kind.RegRel;
            y.reg = Register.Reg.EBP;
            y.inx = null;
            c.PutLEA(x, y);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }

        [TestMethod]
        public void TestLEA_Endianess()
        {
            //lea    ebx,[ebp+esi*4+0x1492672b] 
            byte[] expected = new byte[] { 0x8D, 0x9C, 0xB5, 0x2B, 0x67, 0x92, 0x14 };

            Operand x = new Operand();
            x.reg = Register.Reg.EBX;
            x.kind = Operand.Kind.Reg;
            Operand y = new Operand();
            y.kind = Operand.Kind.RegRel;
            y.inx = Register.Reg.ESI;
            y.reg = Register.Reg.EBP;
            y.scale = 2;
            y.adr = 0x1492672b;
            c.PutLEA(x, y);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }



        [TestMethod]
        public void TestPUSH_Reg()
        {
            //push    ebx 
            byte[] expected = new byte[] { 0x53 };

            Operand x = new Operand();
            x.reg = Register.Reg.EBX;
            x.kind = Operand.Kind.Reg;
            c.PutPUSH(x);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }

        [TestMethod]
        public void TestPUSH_Const()
        {
            //push    0xaabbccdd 
            byte[] expected = new byte[] { 0x68, 0xDD, 0xCC, 0xBB, 0xAA };

            Operand x = new Operand();
            x.val = -1430532899;
            x.kind = Operand.Kind.Con;
            x.type = Tab.intType;
            c.PutPUSH(x);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }


        [TestMethod]
        public void TestPUSH_Mem()
        {
            //push    [ebp+esi*4+0x1492672b]  
            byte[] expected = new byte[] { 0xFF, 0xB4, 0xB5, 0x2B, 0x67, 0x92, 0x14 };

            Operand x = new Operand();
            x.kind = Operand.Kind.RegRel;
            x.inx = Register.Reg.ESI;
            x.reg = Register.Reg.EBP;
            x.scale = 2;
            x.adr = 0x1492672b;
            x.type = Tab.intType;
            c.PutPUSH(x);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }

        [TestMethod]
        public void TestSUB()
        {
            //sub    esp,0x8
            byte[] expected = new byte[] { 0x83, 0xEC, 0x08 };

            Operand x = new Operand();
            x.kind = Operand.Kind.Reg;
            x.reg = Register.Reg.ESP;
            Operand y = new Operand();
            y.kind = Operand.Kind.Con;
            y.type = Tab.intType;
            y.val = 8;
            c.PutDyadic(Code.DyadicOp.sub, x, y);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }

        [TestMethod]
        public void TestMOV()
        {
            //mov    DWORD PTR [esp],0x1
            byte[] expected = new byte[] { 0xC7, 0x04, 0x24, 0x01, 0x00, 0x00, 0x00 };

            Operand x = new Operand();
            x.kind = Operand.Kind.RegRel;
            x.reg = Register.Reg.ESP;
            x.type = Tab.intType;
            Operand y = new Operand();
            y.kind = Operand.Kind.Con;
            y.type = Tab.intType;
            y.val = 1;
            c.PutMOV(x, y);
            CollectionAssert.AreEqual(expected, c.buf.Take(expected.Length).ToArray());
        }


    }
}
